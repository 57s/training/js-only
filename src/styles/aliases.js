const ALIASES = {
	centerFlex: {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	},
};

export default ALIASES;
