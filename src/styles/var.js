const COLORS = {
	bg: '#F8F8F8',
	light: '#FFF',
	dark: '#161515',
	accent: '#BB8B5D',
};

const CONTAINERS = {
	container: '1200px',

	card: {
		width: '300px',
	},
};

export { COLORS, CONTAINERS };
