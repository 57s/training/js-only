const PRODUCT_DESCRIPTION_DATA = [
	{
		id: '1',
		desc: 'Inspired by the WWI trench watch, this timepiece has a contemporary design with an urban style and a vintage soul. The 22 Design Studio Concrete Sector Watch reinterprets the old timepiece with a concrete dial. Handmade in Taiwan, the dial is cast in one piece using the baton technique more detailed',
	},
];
