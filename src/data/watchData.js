import TAGS from '../constants/tags.js';

const WATCH_DATA = [
	{
		id: '1',
		url: './assets/img/watch_1.webp',
		price: 1700,
		title: 'mor705',
		desc: 'The watch also features a tough and durable sapphire a Swiss Solita SW200-1 automatic winding movement that has a power reserve',
		tags: [],
	},

	{
		id: '2',
		url: './assets/img/watch_2.webp',
		price: 2000,
		title: 'Son22',
		desc: 'The watch also features a tough and durable sapphire crystal glass and comes with a Swiss SW200-1 automatic winding movement that has a power reserve.',
		tags: [TAGS.popular],
	},

	{
		id: '3',
		url: './assets/img/watch_3.webp',
		price: 2500,
		title: 'Sw12',
		desc: 'The watch also features a tough and durable sapphire crystal glass and comes with a Sw12-1 automatic winding',
		tags: [TAGS.popular],
	},
];

export default WATCH_DATA;
