function addStylesDom({ elementDom, styles }) {
	Object.entries(styles).forEach(([key, value]) => {
		elementDom.style[key] = value;
	});

	return elementDom;
}

function createDomElement(props) {
	const { dom, text, classList, id, styles, attributes, children, onClick } =
		props || {};

	let element = document.createElement(dom || 'div');

	// Text
	if (text !== undefined) {
		element.textContent = text;
	}

	// Id
	if (id !== undefined) {
		element.id = id;
	}

	// Classes
	if (Array.isArray(classList)) {
		classList.forEach(classItem => element.classList.add(classItem));
	} else if (classList !== undefined) {
		element.classList.add(classList);
	}

	// Styles
	if (styles) addStylesDom({ elementDom: element, styles });

	// Options
	for (const key in attributes) {
		element.setAttribute(key, attributes[key]);
	}

	// children
	if (children) {
		element.appendChild(children);
	}

	// On Click
	if (onClick) {
		element.addEventListener('click', onClick);
	}

	return element;
}

export { createDomElement, addStylesDom };
