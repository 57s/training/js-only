import sectionStyles from './Section.styles.js';

function useConfigSection({ classList, styles }) {
	return {
		configSectionElement: {
			dom: 'section',
			classList: ['section', ...classList],
			styles: { ...sectionStyles, ...styles },
		},
	};
}

export default useConfigSection;
