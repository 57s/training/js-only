import { createDomElement } from '../../../utils/dom.js';
import useConfigSection from './Section.config.js';

function SectionComponent({ classList, styles, children }) {
	const { configSectionElement } = useConfigSection({ classList, styles });

	const sectionElement = createDomElement(configSectionElement);

	sectionElement.appendChild(children);

	return sectionElement;
}

export default SectionComponent;
