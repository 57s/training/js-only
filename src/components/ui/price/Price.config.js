import priceStyles from './Price.styles.js';

function useConfigPrice({ price }) {
	return {
		configPrice: {
			dom: 'div',
			text: `${price} $`,
			classList: ['price'],
			styles: priceStyles,
		},
	};
}

export default useConfigPrice;
