import { createDomElement } from '../../../utils/dom.js';
import useConfigPrice from './Price.config.js';

function PriceComponent(props) {
	const { configPrice } = useConfigPrice({ price: props.price });

	const priceElement = createDomElement(configPrice);

	return priceElement;
}

export default PriceComponent;
