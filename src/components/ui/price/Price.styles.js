import ALIASES from '../../../styles/aliases.js';
import { COLORS } from '../../../styles/var.js';

const priceStyles = {
	...ALIASES.centerFlex,
	minWidth: '140px',
	minHeight: '80px',

	fontWeight: '300',
	fontSize: '32px',
	backgroundColor: COLORS.light,
};

export default priceStyles;
