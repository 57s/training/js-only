import { CONTAINERS } from '../../../styles/var.js';

const containerStyles = {
	maxWidth: CONTAINERS.container,
	width: '90%',
	margin: '0 auto',
};

export default containerStyles;
