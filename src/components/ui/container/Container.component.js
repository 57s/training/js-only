import { createDomElement } from '../../../utils/dom.js';
import containerStyles from './Container.styles.js';
import useConfigContainer from './Container.config.js';

function ContainerComponent(children) {
	const { configElementContainer } = useConfigContainer(children);

	const containerElement = createDomElement(configElementContainer);

	return containerElement;
}

export default ContainerComponent;
