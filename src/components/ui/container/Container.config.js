import containerStyles from './Container.styles.js';

function useConfigContainer(children) {
	return {
		configElementContainer: {
			dom: 'div',
			classList: ['container'],
			styles: containerStyles,
			children,
		},
	};
}

export default useConfigContainer;
