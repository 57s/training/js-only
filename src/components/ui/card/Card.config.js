import cardStyles from './Card.styles.js';

function useConfigCard() {
	return {
		configElementCard: {
			dom: 'div',
			classList: ['card'],
			styles: cardStyles.card,
		},
	};
}

export default useConfigCard;
