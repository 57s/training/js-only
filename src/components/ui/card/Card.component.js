import { createDomElement } from '../../../utils/dom.js';

import useConfigCard from './Card.config.js';
import ImgWrapperComponent from '../imgWrapper/ImgWrapper.component.js';
import TitleComponent from '../title/Title.component.js';
import DescriptionComponent from '../description/Description.component.js';

function CardComponent(watch) {
	const { configElementCard } = useConfigCard();

	const cardElement = createDomElement(configElementCard);
	const imgCardElement = ImgWrapperComponent(watch.url);
	const titleElement = TitleComponent(watch.title);
	const descriptionElement = DescriptionComponent(watch.desc);

	cardElement.appendChild(imgCardElement);
	cardElement.appendChild(titleElement);
	cardElement.appendChild(descriptionElement);

	return cardElement;
}

export default CardComponent;
