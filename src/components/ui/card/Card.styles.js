import { CONTAINERS } from '../../../styles/var.js';

const cardStyles = {
	card: {
		maxWidth: CONTAINERS.card.width,
	},
};

export default cardStyles;
