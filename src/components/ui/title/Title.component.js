import { createDomElement } from '../../../utils/dom.js';
import useConfigTitle from './Title.config.js';

function TitleComponent(text) {
	const { configTitleElement } = useConfigTitle(text);

	const titleElement = createDomElement(configTitleElement);

	return titleElement;
}

export default TitleComponent;
