import titleStyles from './Title.styles.js';

function useConfigTitle(text) {
	return {
		configTitleElement: {
			dom: 'h4',
			text,
			classList: ['card__title'],
			styles: titleStyles,
		},
	};
}

export default useConfigTitle;
