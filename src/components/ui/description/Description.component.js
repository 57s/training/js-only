import { createDomElement } from '../../../utils/dom.js';
import useConfigDescription from './Description.config.js';

function DescriptionComponent(text) {
	const { configDescriptionElement } = useConfigDescription(text);
	const descriptionElement = createDomElement(configDescriptionElement);

	return descriptionElement;
}

export default DescriptionComponent;
