import descriptionStyles from './Description.styles.js';

function useConfigDescription(text) {
	return {
		configDescriptionElement: {
			dom: 'p',
			text,
			classList: ['card__desc'],
			styles: descriptionStyles,
		},
	};
}

export default useConfigDescription;
