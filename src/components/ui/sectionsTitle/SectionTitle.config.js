import sectionTitleStyles from './SectionTitle.styles.js';

function useConfigSectionTitle({ title, subTitle }) {
	return {
		configHGroupElement: {
			dom: 'hgroup',
			classList: ['title-section'],
			styles: sectionTitleStyles.hGroup,
		},

		configSubTitleElement: {
			dom: 'h3',
			text: subTitle,
			classList: ['title-section__sub-title'],
			styles: sectionTitleStyles.supTitle,
		},

		configTitleElement: {
			dom: 'h2',
			text: title,
			classList: ['title-section__title'],
			styles: sectionTitleStyles.title,
		},
	};
}

export default useConfigSectionTitle;
