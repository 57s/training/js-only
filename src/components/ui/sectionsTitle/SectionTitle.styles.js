const sectionTitleStyles = {
	hGroup: {
		marginBottom: '50px',
	},

	supTitle: {
		marginBottom: '20px',

		fontWeight: '300',
		fontSize: '20px',
		textAlign: 'center',
	},

	title: {
		fontSize: '50px',
		textAlign: 'center',
	},
};

export default sectionTitleStyles;
