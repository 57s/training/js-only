import { createDomElement } from '../../../utils/dom.js';
import useConfigSectionTitle from './SectionTitle.config.js';
import titleStyles from './SectionTitle.styles.js';

function TitleSectionComponent(props) {
	const { configHGroupElement, configSubTitleElement, configTitleElement } =
		useConfigSectionTitle(props);

	const hGroupElement = createDomElement(configHGroupElement);
	const subTitleElement = createDomElement(configSubTitleElement);
	const titleElement = createDomElement(configTitleElement);

	hGroupElement.appendChild(subTitleElement);
	hGroupElement.appendChild(titleElement);

	return hGroupElement;
}

export default TitleSectionComponent;
