import ALIASES from '../../../styles/aliases.js';
import { COLORS, CONTAINERS } from '../../../styles/var.js';

const buttonStyles = {
	padding: '25px',
	fontSize: '32px',
	color: COLORS.light,
	backgroundColor: COLORS.dark,
};

export default buttonStyles;
