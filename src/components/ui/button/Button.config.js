import buttonStyles from './Button.styles.js';

function useConfigButton({ label, onClick }) {
	return {
		configButton: {
			dom: 'button',
			text: label,
			classList: ['button'],
			styles: buttonStyles,
			onClick,
		},
	};
}

export default useConfigButton;
