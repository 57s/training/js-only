import { createDomElement } from '../../../utils/dom.js';
import useConfigButton from './Button.config.js';

function ButtonComponent(props) {
	const { configButton } = useConfigButton(props);

	const buttonElement = createDomElement(configButton);

	return buttonElement;
}

export default ButtonComponent;
