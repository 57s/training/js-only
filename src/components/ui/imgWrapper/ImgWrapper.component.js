import { createDomElement } from '../../../utils/dom.js';
import useConfigImgWrapper from './ImgWrapper.config.js';

function ImgWrapperComponent(url) {
	const { configImgWrapper, configImg } = useConfigImgWrapper(url);

	const imgWrapperElement = createDomElement(configImgWrapper);
	const imgElement = createDomElement(configImg);

	imgWrapperElement.appendChild(imgElement);

	return imgWrapperElement;
}

export default ImgWrapperComponent;
