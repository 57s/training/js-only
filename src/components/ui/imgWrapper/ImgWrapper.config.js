import imgWrapperStyles from './ImgWrapper.styles.js';

function useConfigImgWrapper(url) {
	return {
		configImgWrapper: {
			dom: 'div',
			classList: ['img-wrapper'],
			styles: imgWrapperStyles.wrapper,
		},

		configImg: {
			dom: 'img',
			classList: ['img'],
			styles: imgWrapperStyles.img,
			attributes: { src: url, alt: url },
		},
	};
}

export default useConfigImgWrapper;
