import ALIASES from '../../../styles/aliases.js';
import { COLORS, CONTAINERS } from '../../../styles/var.js';

const imgWrapperStyles = {
	wrapper: {
		...ALIASES.centerFlex,
		padding: '50px',
		marginBottom: '40px',
		backgroundColor: COLORS.bg,
		// borderRadius: '10px',
		// boxShadow: `0 0 5px ${COLORS.dark}`,
	},

	img: {
		maxWidth: '200px',
		width: '100%',
		height: 'auto',
	},
};

export default imgWrapperStyles;
