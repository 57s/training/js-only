import { createDomElement } from '../../utils/dom.js';
import appConfig from './App.config.js';

import IntroComponent from '../sections/intro/Intro.component.js';
import PopularComponent from '../sections/popular/Popular.component.js';

function AppComponent() {
	const appElement = createDomElement(appConfig);
	const introElement = IntroComponent();
	const popularElement = PopularComponent();

	appElement.appendChild(introElement);
	appElement.appendChild(popularElement);

	return appElement;
}

export default AppComponent;
