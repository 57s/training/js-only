import appStyles from './App.styles.js';
const appConfig = {
	dom: 'div',

	classList: ['app'],
	styles: appStyles,
};

export default appConfig;
