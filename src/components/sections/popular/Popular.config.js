import popularStyles from './Popular.styles.js';

function useConfigPopular() {
	return {
		configElementInner: {
			dom: 'div',
			classList: ['popular__inner'],
			styles: popularStyles.inner,
		},
	};
}

export default useConfigPopular;
