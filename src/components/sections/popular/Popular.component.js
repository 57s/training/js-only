import ContainerComponent from '../../ui/container/Container.component.js';
import PopularListComponent from '../../blocks/popularList/PopularList.component.js';

import TitleSectionComponent from '../../ui/sectionsTitle/SectionTitle.component.js';
import useConfigPopular from './Popular.config.js';
import { createDomElement } from '../../../utils/dom.js';
import popularStyles from './Popular.styles.js';
import SectionComponent from '../../ui/section/Section.component.js';

function PopularComponent() {
	const { configElementInner } = useConfigPopular();
	const popularElementInner = createDomElement(configElementInner);
	const titleSectionElement = TitleSectionComponent({
		title: 'Models',
		subTitle: 'popular',
	});
	const PopularListElement = PopularListComponent();

	popularElementInner.appendChild(titleSectionElement);
	popularElementInner.appendChild(PopularListElement);

	const containerElement = ContainerComponent(popularElementInner);
	const sectionElement = SectionComponent({
		classList: ['popular'],
		styles: popularStyles.popular,
		children: containerElement,
	});

	return sectionElement;
}

export default PopularComponent;
