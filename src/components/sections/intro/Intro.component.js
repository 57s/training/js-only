import ContainerComponent from '../../ui/container/Container.component.js';
import { createDomElement } from '../../../utils/dom.js';
import useConfigIntro from './Intro.config.js';
import SectionComponent from '../../ui/section/Section.component.js';
import introStyles from './Intro.styles.js';
import WATCH_DATA from '../../../data/watchData.js';
import ButtonComponent from '../../ui/button/Button.component.js';
import PriceComponent from '../../ui/price/Price.component.js';
import testScript from '../../../scripts/testScript.js';

function IntroComponent() {
	const watch = WATCH_DATA[0];

	const {
		configElementIntroInner,
		configElementIntroContent,
		configElementIntroImgBox,
		configElementSubTitle,
		configElementTitle,
		configElementDescription,
		configElementIntroImg,
		configElementBuy,
	} = useConfigIntro({
		imgUrl: watch.url,
		subTitle: watch.title,
		title: 'Watch',
		description: watch.desc,
	});

	const introInnerElement = createDomElement(configElementIntroInner);
	const introContentElement = createDomElement(configElementIntroContent);
	const imgBoxElement = createDomElement(configElementIntroImgBox);
	const subTitleElement = createDomElement(configElementSubTitle);
	const titleElement = createDomElement(configElementTitle);
	const descriptionElement = createDomElement(configElementDescription);
	const imgElement = createDomElement(configElementIntroImg);
	const buyElement = createDomElement(configElementBuy);
	const buttonElement = ButtonComponent({
		label: 'Buy watch',
		onClick: () => testScript('Ку Ку ?'),
	});
	const priceElement = PriceComponent({ price: watch.price });

	introInnerElement.appendChild(introContentElement);
	introInnerElement.appendChild(imgBoxElement);

	introContentElement.appendChild(subTitleElement);
	introContentElement.appendChild(titleElement);
	introContentElement.appendChild(descriptionElement);
	introContentElement.appendChild(buyElement);

	imgBoxElement.appendChild(imgElement);

	buyElement.appendChild(buttonElement);
	buyElement.appendChild(priceElement);

	const containerElement = ContainerComponent(introInnerElement);
	const sectionElement = SectionComponent({
		classList: ['intro'],
		styles: introStyles.intro,
		children: containerElement,
	});

	return sectionElement;
}

export default IntroComponent;
