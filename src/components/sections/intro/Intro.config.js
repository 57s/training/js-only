import introStyles from './Intro.styles.js';

function useConfigIntro({ subTitle, title, description, imgUrl }) {
	return {
		configElementIntroInner: {
			dom: 'div',
			classList: ['intro__inner'],
			styles: introStyles.inner,
		},

		configElementIntroContent: {
			dom: 'div',
			classList: ['intro__content'],
			styles: introStyles.content,
		},

		configElementIntroImgBox: {
			dom: 'div',
			classList: ['intro__img-box'],
			styles: introStyles.imgBox,
		},

		configElementIntroImg: {
			dom: 'img',
			classList: ['intro__img'],
			styles: introStyles.img,
			attributes: { src: imgUrl, alt: subTitle },
		},

		configElementSubTitle: {
			dom: 'h2',
			text: subTitle,
			classList: ['intro__title'],
			styles: introStyles.subTitle,
		},

		configElementTitle: {
			dom: 'h1',
			text: title,
			classList: ['intro__title'],
			styles: introStyles.title,
		},

		configElementDescription: {
			dom: 'p',
			text: description,
			classList: ['intro__description'],
			styles: introStyles.description,
		},

		configElementBuy: {
			dom: 'div',
			classList: ['intro__buy'],
			styles: introStyles.buy,
		},
	};
}

export default useConfigIntro;
