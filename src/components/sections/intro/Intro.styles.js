import ALIASES from '../../../styles/aliases.js';
import { COLORS } from '../../../styles/var.js';

const introStyles = {
	intro: {
		...ALIASES.centerFlex,
		width: `${100 - 2}vw`,
		height: `${100}vh`,
		backgroundColor: COLORS.bg,
	},

	inner: {
		...ALIASES.centerFlex,
	},

	content: {},
	imgBox: {},
	img: {},

	subTitle: {
		fontWeight: '300',
	},

	title: {
		fontSize: '200px',
	},

	description: {
		fontSize: '32px',
		marginBottom: '50px',
	},

	buy: {
		...ALIASES.centerFlex,
	},
};

export default introStyles;
