import { createDomElement } from '../../../utils/dom.js';
import popularListStyles from './PopularList.styles.js';
import CardComponent from '../../ui/card/Card.component.js';
import WATCH_DATA from '../../../data/watchData.js';

import TAGS from '../../../constants/tags.js';
function PopularListComponent() {
	const configElementList = {
		dom: 'div',
		classList: ['popular__list'],
		styles: popularListStyles,
	};

	const popularElementList = createDomElement(configElementList);

	const watchPopular = WATCH_DATA.filter(watch =>
		watch.tags.includes(TAGS.popular)
	);

	watchPopular.forEach(watch => {
		popularElementList.appendChild(CardComponent(watch));
	});

	return popularElementList;
}

export default PopularListComponent;
