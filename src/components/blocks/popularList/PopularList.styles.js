import ALIASES from '../../../styles/aliases.js';

const popularListStyles = {
	...ALIASES.centerFlex,
	flexDirection: 'column',
	gap: '50px',
};

export default popularListStyles;
