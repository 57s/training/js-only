import App from './src/components/app/App.component.js';

import { addStylesDom } from './src/utils/dom.js';
import resetStyles from './src/styles/reset.js';

const bodyElement = document.querySelector('body');
const rootElement = document.querySelector('.root');
const appElement = App();

addStylesDom({ elementDom: bodyElement, styles: resetStyles });

rootElement.appendChild(appElement);
